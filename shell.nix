{ pkgs ? import <nixpkgs> {} }:
  pkgs.mkShell rec {
    # nativeBuildInputs is usually what you want -- tools you need to run
    # nativeBuildInputs = with pkgs.buildPackages; [ pandoc python3Packages.withPackages(ps: with ps; [ pandocfilters ]) ];
  venvDir = "./.venv";
  tex = (pkgs.texlive.combine {
        inherit (pkgs.texlive) scheme-basic
            beamer setspace footnotebackref csquotes mdframed zref needspace sourcesanspro sourcecodepro listings xkeyval ly1 babel-czech;
    });
  buildInputs = [
    pkgs.librsvg
    pkgs.pandoc
    tex
    # A Python interpreter including the 'venv' module is required to bootstrap
    # the environment.
    pkgs.python3Packages.python

    # This execute some shell code to initialize a venv in $venvDir before
    # dropping into the shell
    pkgs.python3Packages.venvShellHook

    # Those are dependencies that we would like to use from nixpkgs, which will
    # add them to PYTHONPATH and thus make them accessible from within the venv.
    # pythonPackages.numpy
    # pythonPackages.requests

    # In this particular example, in order to compile any binary extensions they may
    # require, the Python modules listed in the hypothetical requirements.txt need
    # the following packages to be installed locally:
    # taglib
    # openssl
    # git
    # libxml2
    # libxslt
    # libzip
    # zlib
  ];

  # Run this command, only after creating the virtual environment
  postVenvCreation = ''
    unset SOURCE_DATE_EPOCH
    pip install -r requirements.txt
  '';

  # Now we can execute any commands within the virtual environment.
  # This is optional and can be left out to run pip manually.
  postShellHook = ''
    # allow pip to install wheels
    unset SOURCE_DATE_EPOCH
  '';
}
