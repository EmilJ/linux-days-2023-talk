---
title: "Kde jsou open source čipy?"
author: "Emil J / widlarizer"
subject: "Markdown"
lang: "en"
...

# O čem se budeme bavit

+ Motivaci
+ Softwaru - úlohách a implementacích
+ Výsledcích
+ FPGA, analog, power - dáme stranou
+ [https://gitlab.com/EmilJ/linux-days-2023-talk](https://gitlab.com/EmilJ/linux-days-2023-talk)

# O mě

+ FEL ČVUT
+ LLVM dev @ HighTec EDV-Systeme
+ Diplomka na tohle téma

# Ekonomika

Start small! Jak těžké je začít vývoj integrovaných obvodů?

![Jak těžký to může být? (Inside the Downfall of Doppler Labs, Wired)](img/hardware_business.png){ width=90% }

# Ekonomika: Software

+ Výroba: servery za desítky tisíc. Sekundy, minuty, hodiny
+ Testování: to samé
+ Materiály: nic? Cloud? 
+ Licence: FOSS / cenově dostupné

Nic nového.

# Ekonomika: PCB

<!-- Kdo bastlí PCB? -->

+ Výroba: pár tisíc. 2 dny pragoboard, 2 týdny oživení a testování
+ Materiály: "commercial off-the-shelf" díly
+ Licence: cenově dostupné

![Open source notebook MNT Reform](img/mnt-reform.jpg){ width=60% }

# Ekonomika: Integrovaný obvod

<!-- Někdo umí ASIC? -->

+ Výroba: $15k za wafer, $1M-$10M za výrobní masky. Měsíce.
+ Testování: simulátory, ATPG, drahý software, velké množství měřícího vybavení za miliony
+ Licence: cca stejně jako plat lidí co je používají!

![TSMC wafer, 30cm](img/tsmc-wafer.jpg){ width=70% }

# Jak na cenu výroby

![Pragoboard pool](img/pragoboard.pool.gif){ width=70% }

# Jak na cenu výroby

+ Multi-project wafer
+ Multi-project chip

![MPW (EuroPractice IC Service)](img/mpw.jpg){ width=70% }

# Jak na cenu licencí

+ FOSS!

# Open source čipy

![OpenMPW MPW1 (čip: Matt Venn, foto: Maximo Borga)](img/mpw1_photo.jpg){ width=70% }

# OpenMPW

<!-- Kdo o tom slyšel? -->

+ Výrobce: Skywater (130 nm), Global Foundries (180 nm)
+ Sponzor: Google
+ Výhradně FOSS!
+ Stovky přijatých čipů
+ SoC, FPGA, crypto, ADC... Wordle hra, Sudoku solver, "translate input from the Donkey Kong Jungle Beat bongos into PS2 keystrokes"...
+ Základní RISC-V SoC, zbývá 3mm x 3.6mm pro návrh samotný

# OpenMPW

![OpenMPW MPW2](img/mpw2.png){ width=55% }

# PDK: DRC

![Pragoboard podmínky výroby](img/pragoboard.rules.png){ width=100% }

# PDK: DRC

![SKY130 process stack diagram](img/sky130-pdk-process-stack-diagram.svg){ width=70% }

# PDK: Standardní buňka

<!-- Takže fakt jako návrh PCB! -->

![Invertor, SKY130](img/sky130_fd_sc_hdll__inv_1.svg){ height=70% }

# FOSS EDA

+ EDA = Electronic Design Automation
+ PCB - KiCAD x Altium
+ ASIC - Yosys+OpenROAD+OpenLane x Cadence, Synopsys

# C kompilátory

<!-- Kdo zná kompilátory? -->

+ Jazyk -> strojový kód
+ Frontend pro jazyk (C, Rust...)
+ Optimalizace nad abstraktním IR
+ Backend pro architekturu (x64, ARM...)

![Logo LLVM](img/llvm-wyvern.jpg){ height=40% }

# C kompilátory

![Zdroják](img/godbolt.c.png){ height=70% }

# C kompilátory

![Assembly - pomalé](img/godbolt.s.bad.png){ height=70% }

# C kompilátory

![Assembly - rychlé](img/godbolt.s.png){ height=70% }

# C kompilátory

![CFG - Graf toku řízení](img/godbolt.cfg.png){ height=70% }

# C kompilátory

![Optimalizace před](img/godbolt.before.png){ height=70% }

# C kompilátory

![Optimalizace po](img/godbolt.after.png){ height=70% }

# Co EDA?

+ Jazyk -> geometrie k výrobě čipu
+ Syntéza a minimalizce logiky - kombinační, sekvenční
+ "physical design automation" - technology mapping, umístění a propojení std buněk (hradel atd)
+ Optimalizační úloha: Power, Performance, Area

# Co EDA?

![Návrh digitální techniky](img/physical_design.png){ height=70% }

# Yosys

+ Yosys Open SYnthesis Suite
+ Syntéza a minimalizace, Verilog, technology mapping, formální verifikace
+ Claire Wolfe, TU Wien, YosysHQ GmbH
+ Založené na knihovně ABC
+ 12k commitů, ISC (permisivní)

![Logo Yosys HQ](img/yosyshq.png){ height=10% }

# ABC

+ Syntéza a minimalizace
+ Alan Mishchenko, akademický software z Berkeley
+ AIG, BDD
+ BSD licence

![abc conference paper (Computer Aided Verification: 22nd International Conference, CAV 2010, Edinburgh, UK, July 15-19, 2010)](img/abc.png){ height=70% }

# AIG

+ Vhodný meziformát mezi jinými reprezentacemi - jako IR
+ Vhodné pro SAT

![AIG (wiki)](img/aig.png){ height=50% }

# BDD

+ NP-úplná konstrukce, paměťově účinné, P operace

![BDD (wiki)](img/bdd.png){ height=70% }

# Co dál?

+ Přeskakuju sekvenční syntézu

![Fyzický návrh](img/physical_design.png){ height=70% }

# OpenROAD

+ Placement, routing, DRC, clock tree synthesis, timing analysis...
+ "Industrial-strength", ale ne všechny features co konkurence
+ Projekt DARPA
+ Univerzity, korporáty. S. Amerika, Asie
+ 18k commitů, BSD

![Logo OpenROAD](img/openroad.png){ height=10% }

# Placement

+ Cíl: rozmístit std buňky
+ State of the art: pseudo elektrostatické solvery - ePlace, RePlace
+ Alternativně: simulované ochlazování, partitioning

![Lu, Jingwei, et al. "ePlace: Electrostatics based placement using Nesterov's method." Proceedings of the 51st Annual Design Automation Conference. 2014.](img/eplace.png){ height=40% }

<!-- # Placement -->

# Placement

![CTU CAN FD IP, před](img/canfd-place-init-inv.png){ height=70% }

# Placement

![CTU CAN FD IP, průběh](img/canfd-place-mid-inv.png){ height=70% }

# Placement

![CTU CAN FD IP, po](img/canfd-place-done-inv.png){ height=70% }

# Routing

+ Jak spojit síť spojených vrcholů kolmými čarami?
+ Jako min. kostra váženého grafu, ale přidávám vrcholy
+ = Steinerovy stromy, ale "rektilineární"
+ NP-úplné, heuristiky
+ Fyzicky omezená "kapacita" pro routing
+ Vliv na rychlost, výkon
+ Kolize - "rip up and reroute", A\*, komplexní DRC podmínky

# Routing

![TT02 Chase the beat, 1 vrstva](img/chase-route-mixed-direction-inv.png){ height=70% }

# Routing

![TT02 Chase the beat, RL Steiner](img/chase-route-3dsteiner-inv.png){ height=70% }

# OpenLane

+ plný automatizovatelný workflow
+ Stojí na něm OpenMPW
+ eFabless Corp, American University in Cairo
+ 1800 commitů, Apache

![OpenLane](img/openlane.flow.png){ height=50% }

# TinyTapeout

+ FOSS only, multi-project chip
+ 1 návrh, 1x ASIC + PCB: $100 + poštovné
+ TT05 - Deadline odevzdání je za 27 dní (4.11.2023)
+ TT02 - 165 návrhů na 1 čipu, deadline 2.12.2022, PCB snad v prosinci 2023

![TinyTapeout 2, detail 6 návrhů](img/tt02-close.png){ height=40% }

# TinyTapeout

![Wokwi](img/wokwi.png){ height=70% }

# Závěr

+ Gde čipy? Tady a teď
+ VLSI EDA je plná NP-úplných úloh, zbývá spousta práce
+ Ale např LLVM má 16x víc commitů než Yosys+OpenROAD
+ Příležistosti pro bastlení, výzkum, podnikání, vzdělávání
+ Zkuste to, pokud můžete

# Jak se do toho pustit

+ [youtube.com/@ZeroToASICcourse](https://youtube.com/@ZeroToASICcourse)
+ [open-source-silicon.slack.com](https://open-source-silicon.slack.com)
+ [tinytapeout.com](https://tinytapeout.com/)
+ #yosyshq na [1bitsquared.com/pages/chat](https://1bitsquared.com/pages/chat)
+ Pull requesty, sponsors

# Díky za pozvání

+ [@the_art_of_giving_up@mastodon.social](mastodon.social/@the_art_of_giving_up)

